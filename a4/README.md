> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Enrique Toledo

### Assignment 4 Requirements:

*Three Parts:*

1. Create Create a database for an office supply company
2. Create ERD showing relationships between tables

#### README.md file should include the following items:

* Screenshot of ERD

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| *Screenshot of my code part ERD*:          |
|------------------------------------------|
| ![Screenshot of ERD](img/erd.png) |

