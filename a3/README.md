> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Enrique Toledo

### Assignment 3 Requirements:

*Three Parts:*

1. Screenshot of my SQL code
2. Screenshot of my populated tables
3. Questions for ch.12

#### README.md file should include the following items:

* Create and populate Oracle tables
* Screenshot of my SQL code
* Screenshot of my populated tables
* Questions for ch.12

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| *Screenshot of my code part 1*:          | *Screenshot of my code part 2*:          |
|------------------------------------------|------------------------------------------|
| ![Screenshot of SQL code](img/code1.png) | ![Screenshot of SQL code](img/code2.png) |

*Screenshot of my populated tables*:

![Screenshot of populateed tables](img/populated_tables.png)
