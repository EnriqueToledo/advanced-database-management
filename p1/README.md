> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Enrique Toledo

### Project 1 Requirements:

*Five Parts:*

1. Create database that documents City's court cases
2. Create ERD 
3. Backwards engineer ERD
4. Hash social security numbers
5. Screenshot of ERD

#### README.md file should include the following items:

* Screenshot of ERD
* Questions for ch.13

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| *Screenshot of my ERD*:              | 
|--------------------------------------|
| ![Screenshot of ERD](img/p1_erd.png) |


