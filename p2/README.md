> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Enrique Toledo

### Project 2 Requirements:

*Three Parts:*

1. Install MongoDB
2. import primer-dataset.json to bin directory
3. Run queries from restaurants collection in test db

#### README.md file should include the following items:

* Screenshot of at least one MongoDB shell command

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| *Screenshot of MongoDb shell commands*: |                                     |
|-----------------------------------------|-------------------------------------|
| ![Screenshot of img1](img/img1.png)     | ![Screenshot of img2](img/img2.png) |


