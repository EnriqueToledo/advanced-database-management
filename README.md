> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Management

## Enrique Toledo

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create bitbucket repo
    - Complete Bitbucket tutorial (bitbucket station locations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create local admin user1 & user2
    - Screenshot of SQL code
    - Screenshot of populated tables

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create and populate Oracle tables
    - Create customer, commodity, & "order" table
    - Screenshot of my SQL code
    - Screenshot of my populated tables

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create database that documents City's court cases
    - Create ERD 
    - Backwards engineer ERD
    - Hash social security numbers
    - Screenshot of ERD

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Create a database for an office supply company
    - Create ERD showing relationships between tables

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Expand on a4, by creating Region, State, City, and store tables
    - Provide more detailed sales reports based on Product, Customer, etc. tables

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Install MongoDB
    - import primer-dataset.json to bin directory
    - Run queries from restaurants collection in test db
 