> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Enrique Toledo

### Assignment 2 Requirements:

*Three Parts:*

1. Screenshot of my SQL code
2. Screenshot of my populated tables
3. Questions for ch.11

#### README.md file should include the following items:

* Screenshot of my SQL code
* Screenshot of my populated tables
* Questions for ch.11

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of my SQL code part 1*:

![Screenshot of SQL code](img/code1.png)

*Screenshot of my SQL code part 2*:

![Screenshot of SQL code](img/code2.png)

*Screenshot of my populated tables*:

![Screenshot of populateed tables](img/tables.png)
